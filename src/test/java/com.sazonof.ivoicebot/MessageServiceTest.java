package com.sazonof.ivoicebot;

import com.sazonof.ivoicebot.service.MessageService;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MessageServiceTest {
    private MessageService messageService = new MessageService();
    private final String example = "some question";

    @Test
    public void successCallGetAnswer() {
        String answer = messageService.getAnswer(example);
        assertNotNull(answer);
    }

    @Test
    public void sendNullToGetAnswer() {
        boolean actual = true;
        boolean expected = false;
        try {
            messageService.getAnswer(null);
        } catch (NullPointerException e) {
            actual = false;
        }

        assertEquals(actual, expected);
    }
}
