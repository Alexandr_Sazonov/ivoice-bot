package com.sazonof.ivoicebot.service;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpEntities;
import akka.http.javadsl.model.HttpRequest;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

public class MessageService {
    private final static String YES = "y";
    private final static String API_URL = "https://odqa.demos.ivoice.online/model";
    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final String RULE_REGEX = "\"";
    private final static String json = "{\n" +
            "  \"context\": [\n" +
            "    \"%s\"\n" +
            "  ]\n" +
            "}";

    public boolean nextQuestion(String message) {
        return message.equalsIgnoreCase(YES) ? true : false;
    }

    public String getAnswer(String question) {
        if (question == null) {
            throw new NullPointerException("Question equals null");
        }
        final ActorSystem system = ActorSystem.create();
        final Materializer materializer = ActorMaterializer.create(system);
        final Http http = Http.get(system);
        StringBuffer answer = new StringBuffer();
        AtomicBoolean finish = new AtomicBoolean(false);

        final HttpRequest httpRequest =
                HttpRequest.POST(API_URL)
                        .withEntity(HttpEntities.create(ContentTypes.APPLICATION_JSON, String.format(json, question).getBytes()));

        Source.single(httpRequest)
                .mapAsync(1, http::singleRequest)
                .runWith(Sink.foreach(response -> {
                    response.entity().getDataBytes().runForeach(value ->
                        answer.append(value.decodeString(DEFAULT_CHARSET).split(RULE_REGEX)[1]), materializer)
                            .thenAccept(done -> {
                                system.terminate();
                                finish.set(true);
                            });
                }), materializer);

        do {
            try {
                TimeUnit.MILLISECONDS.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException();
            }
        } while (!finish.get());

        return answer.toString();
    }
}
