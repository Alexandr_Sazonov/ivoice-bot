package com.sazonof.ivoicebot.repository.impl;

import com.sazonof.ivoicebot.repository.MessageRepository;
import com.sazonof.ivoicebot.repository.exception.ConsoleFailException;
import com.sazonof.ivoicebot.util.BotMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleMessageRepository implements MessageRepository {
    private BufferedReader reader;

    public String getMessage() {
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            return reader.readLine();
        } catch (IOException e) {
            throw new ConsoleFailException(BotMessage.FAIL_READ, e);
        }
    }
}
