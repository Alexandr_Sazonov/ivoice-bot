package com.sazonof.ivoicebot.repository.exception;

public class ConsoleFailException extends RuntimeException {

    public ConsoleFailException() {
        super();
    }

    public ConsoleFailException(String m, Exception e) {
        super(m, e);
    }
}
