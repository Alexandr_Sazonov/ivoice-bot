package com.sazonof.ivoicebot.repository;

public interface MessageRepository {
    String getMessage();
}
