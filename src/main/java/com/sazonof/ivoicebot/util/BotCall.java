package com.sazonof.ivoicebot.util;

public class BotCall {

    public static void ask(){
        System.out.println("Bot: " + BotMessage.ASK_QUESTION );
    }

    public static void askNext() {
        System.out.println("Bot: " + BotMessage.ASK_NEXT_QUESTION);
    }

    public static void userPrefix() {
        System.out.print("User: ");
    }

    public static void answer(String message) {
        System.out.println(message);
    }
}
