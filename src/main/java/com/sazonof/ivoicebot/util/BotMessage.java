package com.sazonof.ivoicebot.util;

public interface BotMessage {
    String FAIL_READ = "Can not read console, please try later";
    String ASK_QUESTION = "Please, ask your question";
    String ASK_NEXT_QUESTION = "Ask next question? Y/n";
}
