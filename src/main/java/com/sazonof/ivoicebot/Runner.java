package com.sazonof.ivoicebot;


import com.sazonof.ivoicebot.repository.MessageRepository;
import com.sazonof.ivoicebot.repository.impl.ConsoleMessageRepository;
import com.sazonof.ivoicebot.service.MessageService;
import com.sazonof.ivoicebot.util.BotCall;

public class Runner {

    public static void main(String[] args) throws Exception {
        final MessageRepository repository = new ConsoleMessageRepository();
        final MessageService service = new MessageService();

        boolean exit = false;

        do {
            BotCall.ask();
            BotCall.userPrefix();
            String question = repository.getMessage();
            String answer = service.getAnswer(question);

            System.out.println(answer);
            BotCall.askNext();
            question = repository.getMessage();
            exit = service.nextQuestion(question);
        } while (exit);
    }
}

